# Single GCLB - external HTTP proxy - and standalone negs

## Purpose
Testing a single GCLB pointing to backend running on multiple GKE clusters in more than 1 region.
Docs - [link](https://cloud.google.com/kubernetes-engine/docs/how-to/standalone-neg#using-gcloud-config)

## Ho to deploy
### 1 Create the GKE clusters
```
gcloud container clusters create london \
    --enable-ip-alias \
    --create-subnetwork="" \
    --network=default \
    --zone=europe-west2-a
    --release-channel=regular

gcloud container clusters create frankfurt \
    --enable-ip-alias \
    --create-subnetwork="" \
    --network=default \
    --zone=europe-west2-a
    --release-channel=regular
```

### 2 Apply the manifests to the clusters
```
gcloud container clusters get-credentials frankfurt --zone europe-west4-a
kubectl apply -f .
gcloud container clusters get-credentials london --zone europe-west2-a
kubectl apply -f .
```

### 3 Create the GCLB
  1- create the firewall rule for the healthchecks
```
gcloud compute firewall-rules create fw-allow-health-check-and-proxy \
  --network=default \
  --action=allow \
  --direction=ingress \
  --source-ranges=130.211.0.0/22,35.191.0.0/16 \
  --rules=tcp:9376
```

  2- create the VIP
```
gcloud compute addresses create hostname-server-vip \
  --ip-version=IPV4 \
  --global
```

  3- create the healthcheck
```
gcloud compute health-checks create http http-basic-check \
  --use-serving-port
```

  4- create a backend service
```
gcloud compute backend-services create my-bes \
  --protocol HTTP \
  --health-checks http-basic-check \
  --global
```

5- create **url-map** and **target proxy**
```
gcloud compute url-maps create web-map \
  --default-service my-bes

gcloud compute target-http-proxies create http-lb-proxy \
  --url-map web-map
```

6- create a forwarding rule
```
gcloud compute forwarding-rules create http-forwarding-rule \
  --global \
  --target-http-proxy=http-lb-proxy \
  --ports=80
```

7- add the NEG to the backedn service
```
gcloud compute backend-services add-backend my-bes --global \
  --network-endpoint-group app-neg \
  --network-endpoint-group-zone europe-west2-b \
  --balancing-mode RATE --max-rate-per-endpoint 5

gcloud compute backend-services add-backend my-bes --global \
--network-endpoint-group app-neg \
--network-endpoint-group-zone europe-west4-c \
--balancing-mode RATE --max-rate-per-endpoint 5
```

